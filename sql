------------------câu 1.count number of orders that have customer name = 'Peter'--------
SELECT c.name ,count(o.customer_id) as sum_customer
FROM customer c, orders o
WHERE c.customer_id = o.customer_id and name = 'HEOS'
GROUP BY c.customer_id
ORDER by c.name desc

-------------------câu 2.count number of orders of employee = 'Giz'---------------------
SELECT e.name ,count(o.employee_id) as sum_employee
FROM employee e, orders o
WHERE e.employee_id = o.employee_id and name = 'mataa'
GROUP BY e.employee_id
ORDER by e.employee_id desc 

-------------------câu 3.query employees that have more than two orders------------------
SELECT e.name, count(o.employee_id) as soluong
FROM employee e , orders o
where o.employee_id = e.employee_id
group by e.employee_id
HAVING count(o.employee_id) >1

--------------------câu 4. top 3 employees in this month (# orders)----------------------
SELECT e.name,count(o.employee_id) as sodonhang
FROM employee e
inner join orders o
on o.employee_id = e.employee_id
where o.date BETWEEN '2021/10/1' and '2021/10/31'
group by e.name
ORDER by e.name desc
LIMIT 3

--------------------câu 5. top 3 products most sell in this month------------------------
SELECT p.name,count(p.product_id) as tongsoluong
FROM product p 
inner join orders o
on p.product_id = o.product_id
where o.date BETWEEN '2021/10/1' and '2021/10/31'
group by p.name
ORDER by p.name asc
LIMIT 3

---------------------câu 6. Find employees that have most customers------------------------
select e.name, kh.SLKH
from employee e
inner join (
	select o.employee_id,
			count(distinct o.customer_id) as SLKH
	from orders o
	group by o.employee_id
	order by SLKH desc
			) as kh
on e.employee_id = kh.employee_id
order by kh.SLKH desc
limit 1
---------------------câu 7. Lấy ra tất cả nhân viên có số lượng khách hàng nhiều nhất ----------- ranḳ()

with sum_employee as
(
	select e.employee_id,count(distinct o.customer_id) as soluongKH
	from orders o ,employee e
	where e.employee_id= o.employee_id
	group by e.employee_id
	order by soluongKH desc
)
select rank_e.employee_id, e.name, soluongKH,rank_number
from(
        select s.employee_id,soluongKH,
            rank() over (order by soluongKH desc)
					as rank_number
        from sum_employee s
   	) as rank_e , employee e 
where e.employee_id = rank_e.employee_id  -- and rank_number = 4

 